//
//  CardDesignView.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/24/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

 @IBDesignable  class CardDesignView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    @IBInspectable var maskBound: Bool = false
    @IBInspectable var borderColor: UIColor = UIColor.white
    @IBInspectable var borderWidth: CGFloat = 0.00
    
    override  func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        
        layer.masksToBounds = maskBound
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

@IBDesignable class CardDesignButton : UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var cardTitle: String = "Button" {
        didSet {
            self.setTitle(cardTitle, for: .normal)
        }
    }

    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    @IBInspectable var maskBound: Bool = false
    @IBInspectable var borderColor: UIColor = UIColor.white
    @IBInspectable var borderWidth: CGFloat = 0.00
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        
        layer.masksToBounds = maskBound
        layer.shadowColor   = shadowColor?.cgColor
        layer.shadowOffset  = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }

}
