//
//  PreloginModel.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/27/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

// OTP verification response message
 struct OTPVerificationResponse : Codable {
    let code : Int?
    let data : String?
    let msg : String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case msg = "Msg"
    }
}

//MARK:- PreLogin Request
 struct PreLoginRequestModel: Codable {
    let agentOtpRequest: PreLoginAgentOtpRequest?
    let isSendOtp: Bool?
    let osVersion: String?
    let login: PreLoginLogin?
    let appId: String?
    
    enum CodingKeys: String, CodingKey {
        case agentOtpRequest = "AgentOtpRequest"
        case isSendOtp = "IsSendOtp"
        case osVersion = "OsVersion"
        case login = "Login"
        case appId = "AppId"
    }
}

 struct PreLoginAgentOtpRequest: Codable {
    let agentCode, clientIp, requestType, clientOs: String?
    
    enum CodingKeys: String, CodingKey {
        case agentCode = "AgentCode"
        case clientIp = "ClientIp"
        case requestType = "RequestType"
        case clientOs = "ClientOs"
    }
}

 struct PreLoginLogin: Codable {
    let mobileNumber, otp, simid, ostype: String?
    let msid: String?
    
    enum CodingKeys: String, CodingKey {
        case mobileNumber = "MobileNumber"
        case otp = "Otp"
        case simid = "Simid"
        case ostype = "Ostype"
        case msid = "Msid"
    }
}

// Prelogin response model
 struct PreLoginResponse: Codable {
    let data: String?
    let code: Int?
    let msg: String?
    
    enum CodingKeys: String, CodingKey {
        case data = "Data"
        case code = "Code"
        case msg = "Msg"
    }
}

 struct PreLoginDataResponse: Codable {
    let language, passwordType, estelResponse: String?
    
    enum CodingKeys: String, CodingKey {
        case language = "Language"
        case passwordType = "PasswordType"
        case estelResponse = "EstelResponse"
    }
}

struct LoginAccess {
    var mobileNumber : String?
    var countryCode : Country?
    var isPattern : Bool?
    
    init(mobile: String, country: Country, isPattern: Bool) {
        self.mobileNumber = mobile
        self.countryCode  = country
        self.isPattern    = isPattern
    }
    
    mutating func updateMobileNumber(_ number: String) -> LoginAccess {
        self.mobileNumber = number
        return self
    }
    
   mutating func updatePattern(_ pattern: Bool) -> LoginAccess {
        self.isPattern = pattern
        return self
    }
    
    mutating func saveModel() {
        OKPayConstants.global.defaultSet.setValue(self.mobileNumber, forKey: "loginNumber")
        OKPayConstants.global.defaultSet.setValue(self.countryCode?.dialCode, forKey: "loginCountry")
        OKPayConstants.global.defaultSet.setValue(self.countryCode?.code, forKey: "loginCountryName")
        OKPayConstants.global.defaultSet.setValue(self.isPattern, forKey: "loginPatter")
        OKPayConstants.global.defaultSet.synchronize()
    }
    
    static func retrieveModel() -> (number: String, code: String, isPattern: Bool, name: String) {
        let number  = OKPayConstants.global.defaultSet.string(forKey: "loginNumber") ?? ""
        let country = OKPayConstants.global.defaultSet.string(forKey: "loginCountry") ?? ""
        let countryName = OKPayConstants.global.defaultSet.string(forKey: "loginCountryName") ?? ""
        let pattern = OKPayConstants.global.defaultSet.bool(forKey: "loginPatter")
        return (number, country, pattern, countryName)
    }
    
}
